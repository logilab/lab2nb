import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { NbButton } from './nbButton';

/**
 * Initialization data for the jupyterlab2jupyterNb extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: 'jupyterlab2jupyterNb:plugin',
  autoStart: true,
    activate: (app: JupyterFrontEnd) => {
        app.docRegistry.addWidgetExtension('Notebook', new NbButton());
        console.log('JupyterLab extension jupyterlab2jupyterNb is activated!');
  }
};

export default extension;
