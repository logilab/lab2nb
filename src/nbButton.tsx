import { DocumentRegistry } from '@jupyterlab/docregistry';
import { INotebookModel, NotebookPanel } from '@jupyterlab/notebook';
import { DisposableDelegate, IDisposable } from '@lumino/disposable';
import { ToolbarButton } from '@jupyterlab/apputils';
import { LabIcon } from '@jupyterlab/ui-components';
import jnbSvgStr from '../style/logo.svg';

export class NbButton
  implements DocumentRegistry.IWidgetExtension<NotebookPanel, INotebookModel> {
  public createNew(panel: NotebookPanel): IDisposable {
    const callback = () => {
      this.nbButton();
    };
    const button = new ToolbarButton({
      icon: new LabIcon({
        name: 'lab2nb',
        svgstr: jnbSvgStr
      }),
      onClick: callback,
      tooltip: 'Open current notebook in classic jupyter notebook'
    });

    panel.toolbar.insertItem(0, 'notebook', button);
    return new DisposableDelegate(() => {
      button.dispose();
    });
  }

  protected nbButton = () => {
    var url = window.location.href;
    window.open(url.replace('/lab/tree/', '/notebooks/'));
  };
}
